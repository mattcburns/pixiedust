import json

from flask import Flask, jsonify

app = Flask(__name__)


@app.route('/v1/boot/<mac>')
def boot(mac):

    config = db.get(mac)
    if config is None:
        return jsonify(db['default'])
    app.logger.info(json.dumps(config))
    return jsonify(config)

if __name__ == "__main__":
    with open('static-config.json', 'r') as f:
        db = json.load(f)

    app.run(host='0.0.0.0', port=5000, debug=True)